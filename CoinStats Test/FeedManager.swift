//
//  FeedManager.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import CoreData

class FeedManager {
    
    // MARK: - Variables
    static let shared = FeedManager()
    var request = NSFetchRequest<FeedCD>(entityName: "FeedCD")
    private init() { }
    
    // MARK: - Insert New
    func insert(feed: Feed) throws {
        let item = FeedCD(context: CoreDataManager.shared.context)
        item.category = feed.category
        item.title = feed.title
        item.body = feed.body
        item.shareUrl = feed.shareUrl
        item.coverPhotoUrl = feed.coverPhotoUrl
        item.date = String(feed.date ?? 0)
        item.id = item.date
        item.isSeen = feed.isSeen
    }
    
    func save() throws {
        do {
            try CoreDataManager.shared.save()
        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: - Get items
    func fetchAll() throws -> [FeedCD] {
        let items = try CoreDataManager.shared.context.fetch(FeedCD.fetchRequest() as NSFetchRequest<FeedCD>)
        return items
    }

    func fetch(withID itemID: String) throws -> [FeedCD] {
        request = NSFetchRequest<FeedCD>(entityName: "FeedCD")
        request.predicate = NSPredicate(format: "id == %@", itemID)
        let items = try CoreDataManager.shared.context.fetch(request)
        return items
    }
    
    // MARK: - Update Item
    func update(item: FeedCD) throws {
        try! save()
    }
    
    // MARK: - Delete Item
    func delete(item: FeedCD) throws {
        CoreDataManager.shared.context.delete(item)
    }
    
    func deleteItem(withID itemID: String) throws {
        let items = try! fetch(withID: itemID)
        if items.count > 0 {
            try! delete(item: items[0])
        }
    }
    
    func deleteAll() {
        let all = try! fetchAll()
        for item in all {
            try! delete(item: item)
        }
    }
}

