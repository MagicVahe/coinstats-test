//
//  CoreDataManager.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import CoreData

class CoreDataManager {
    // MARK: - Variables
    static let shared = CoreDataManager()
    private init(){
        initializeStack()
    }
    let persistentContainer = NSPersistentContainer(name: "CoinStats_Test")
    // MARK: - Initialize
    func initializeStack() {
        self.persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                print("could not load store \(error.localizedDescription)")
                return
            }
            print("store loaded")
        }
    }
    
    var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    func save() throws {
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
}
