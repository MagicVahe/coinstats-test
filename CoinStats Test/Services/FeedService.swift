//
//  FeedService.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import Foundation

protocol FeedServiceProtocol {
    
    func get(completion: @escaping ((FeedReponseData<FeedResult>) -> Void))
}

class FeedService: FeedServiceProtocol {
    
    func get(completion: @escaping ((FeedReponseData<FeedResult>) -> Void)) {
        let params = ["": ""]
        
        BaseService.shared.get(endpoint: "/feed", parameters: params, for: .unsecure).responseString(encoding: String.Encoding.utf8) { (response) in
            if let respponse = response.response, let result = response.result.value {
                if  let baseResponse = BaseService.shared.checkBaseResponse(respponse, result){
                    return completion(.base(response: baseResponse))
                }
            }
            
            switch response.response?.statusCode ?? 400 {
            case 200:
                do {
                    if let value = response.result.value {
                        return completion(.success(data: try JSONDecoder().decode(FeedResult.self, from: Data(value.utf8))))
                    }
                } catch {
                    return completion(.base(response: .unexpectedError(error: ResponseUnexpectedError.mappingFailed)))
                }
            default:
                return completion(.base(response: .badRequest))
            }
        }
    }
    
}
