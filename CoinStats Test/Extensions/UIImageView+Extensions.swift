//
//  UIImageView+Extensions.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import AlamofireImage
import Alamofire

extension UIImageView {
    
    func getImageFromURL(_ path: String) {
        if let url = URL(string: path) {
            self.af_setImage(withURL: url, placeholderImage: UIImage(named: "placeholder"))
        }
    }
}
