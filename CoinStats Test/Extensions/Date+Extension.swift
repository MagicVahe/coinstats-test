//
//  Date+Extension.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import UIKit

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    var seconds:Int {
        return Int(self.timeIntervalSince1970)
    }
//    init(milliseconds:Int64) {
//        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
//    }
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    init(seconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(seconds))
    }
}
