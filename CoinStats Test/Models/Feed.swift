//
//  Feed.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import Foundation

class FeedResult: Codable {
    
    var success: Bool
    var data: [Feed]?
}

class Feed: Codable {
    var category: String?
    var title: String?
    var body: String?
    var shareUrl: String?
    var coverPhotoUrl: String?
    var date: Int?
    var gallery: [FeedGallery]?
    var video: [FeedVideo]?
    var isSeen = false
    
    enum CodingKeys: String, CodingKey {
        case category
        case title
        case body
        case shareUrl
        case coverPhotoUrl
        case date
        case gallery
        case video
    }
}

class FeedGallery: Codable {
    var title: String?
    var thumbnailUrl: String?
    var contentUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case thumbnailUrl
        case contentUrl
    }
}

class FeedVideo: Codable {
    var title: String?
    var thumbnailUrl: String?
    var youtubeId: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case thumbnailUrl
        case youtubeId
    }
}

enum FeedReponseData<FeedResult> {
    case success(data: FeedResult)
    case base(response: BaseResponse)
}
