//
//  DateFormattingHelper.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation

enum DateFormat: String {
    case ShortDate = "MM/dd/yy"
    case LongDate = "MMMM dd, yyyy"
    case EventClientDate = "dd MMMM yyyy"
    case ShortTime = "HH.mm"
    case ShortDataRevers = "dd/MM/yy"
    case HoursAndSeconds = "HH:mm"
    case StandartDate = "yyyy-MM-dd"
    case specific = "d MMM yyyy"
    case dayAndManth = "dd MMMM HH:mm"
    case midleDay = "dd MMM"
    case timeAmPm = "h:mma"
}

enum DateFormatterFormats: String {
    case long = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case middle = "yyyy-MM-dd'T'HH:mm:ss"
    case short = "yyyy-MM-dd"
}

class DateFormattingHelper {
    
    static private let formatter = DateFormatter()
    
    static let formatterForParse = { (_ format: DateFormatterFormats ) -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter
    }
    
    static func stringFrom(date: Date?, format: DateFormat) -> String {
        guard let date = date else {
            return ""
        }
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date)
    }
    
    static func dateFrom(string: String, format: DateFormat) -> Date {
        formatter.dateFormat = format.rawValue
        return formatter.date(from: string) ?? Date()
    }
    
    static func dateFromSecond(string: String, format: DateFormat) -> Date {
        formatter.dateFormat = format.rawValue
        return formatter.date(from: string) ?? Date()
    }
    
    static func showTime(secondTime: Int) -> String {
        let time = Date()
        let second = time.seconds
        if second - (secondTime ) < 60 {
            return NSLocalizedString("Just now", comment: "")
        } else if second - (secondTime ) > 60 && second - (secondTime ) < 3600 {
            return "\((second - (secondTime ))/60) \(NSLocalizedString("min ago", comment: ""))"
        } else {
            return stringFrom(date: Date(seconds: secondTime), format: DateFormat.midleDay) + ", " + stringFrom(date: Date(seconds: secondTime), format: DateFormat.timeAmPm).lowercased()
        }
    }
}
