//
//  DetaileViewController.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import UIKit
import SafariServices

class DetaileViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var detaileViewObject: DetaileViewObject!
    
    @IBOutlet weak var fullImageView: UIView!
    @IBOutlet weak var fullImage: UIImageView!
    
    // MARK: - Variables
    var viewModel: DetaileViewModel!
    private var showMore = true
    private let collectionViewHeight = 100.0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initCell()
        configureView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        displayImageView.contentMode = .scaleAspectFill
        let bounds = UIScreen.main.bounds
        imageHeightConstraint.constant = (displayImageView.image?.size.height ?? 0)*bounds.width/(displayImageView.image?.size.width ?? 0)
    }
    
    // MARK: - Config Init
    func initCell() {
        collectionView.register(UINib(nibName: GalleryCell.identifire, bundle: nil),
                                               forCellWithReuseIdentifier: GalleryCell.identifire)
        detaileViewObject.collectionView.register(UINib(nibName: VideoCell.identifire, bundle: nil),
                                               forCellWithReuseIdentifier: VideoCell.identifire)
    }
    
    private func configureView() {
        self.title = viewModel.item.category
        displayImageView.getImageFromURL(viewModel.item.coverPhotoUrl ?? "")
        let str = (viewModel.item.title ?? "") + "\n" + DateFormattingHelper.showTime(secondTime: viewModel.item.date ?? 0)
        titleLabel.attributedText = createAttr(fullString: str, changedString: DateFormattingHelper.showTime(secondTime: viewModel.item.date ?? 0))
        bodyLabel.attributedText = viewModel.item.body?.htmlToAttributedString
        configureButton(isMore: showMore)
        if let videos = viewModel.item.video {
            detaileViewObject.items = videos
        } else {
            detaileViewObject.items = []
        }
        detaileViewObject.collectionView.reloadData()
        detaileViewObject.delegate = self
        fullImageView.isHidden = true
    }
    
    private func createAttr(fullString: String, changedString: String) -> NSMutableAttributedString{
        let attribute = NSMutableAttributedString.init(string: fullString)
        let range = (fullString as NSString).range(of: changedString)
        attribute.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica", size: 14)!, range: range)
        return attribute
    }

    private func configureButton(isMore: Bool) {
        if isMore {
            let text = "\(NSLocalizedString("show more", comment: ""))..."
            showMoreButton.setAttributedTitle( setAttributeButton(text: text), for: UIControl.State.normal)
            bodyLabel.numberOfLines = 3
        } else {
            let text = "\(NSLocalizedString("show less", comment: ""))..."
            showMoreButton.setAttributedTitle( setAttributeButton(text: text), for: UIControl.State.normal)
            bodyLabel.numberOfLines = 0
        }
    }
    
    private func setAttributeButton(text: String) -> NSMutableAttributedString {
        let string = text
        let attribute = NSMutableAttributedString.init(string: string)
        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: (string as NSString).range(of: string))
        return attribute
    }
    
    private func openSafariVC(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        let savariVC = SFSafariViewController(url: url)
        self.present(savariVC, animated: true, completion: nil)
    }
    
    private func showMessage(title: String?, message: String?) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Actions
    @IBAction func showMore(_ sender: Any) {
        showMore = !showMore
        configureButton(isMore: showMore)
    }
    @IBAction func closeFullImage(_ sender: Any) {
        fullImageView.isHidden = true
        fullImage.image = nil
    }
}

extension DetaileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let gallery = viewModel.item.gallery {
            collectionViewHeightConstraint.constant = collectionViewHeight
            return gallery.count
        }
        collectionViewHeightConstraint.constant = 0
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCell.identifire, for: indexPath) as! GalleryCell
        if let gallery = viewModel.item.gallery {
            cell.item = gallery[indexPath.row]
        }
        return cell
    }
}

extension DetaileViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let gallery = viewModel.item.gallery {
            fullImage.getImageFromURL(gallery[indexPath.row].contentUrl ?? "")
            fullImageView.isHidden = false
        }
        
    }
}

extension DetaileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewHeightConstraint.constant, height: collectionViewHeightConstraint.constant)
    }
}

extension DetaileViewController: DetaileViewObjectDelegate {
    func openVideo(_ path: String) {
        if path.contains("http") {
            openSafariVC(with: path)
        } else {
            showMessage(title: "OOPS", message: "")
        }
    }
}
