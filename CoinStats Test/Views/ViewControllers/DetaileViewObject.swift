//
//  DetaileViewObject.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import UIKit

protocol DetaileViewObjectDelegate: AnyObject {
    func openVideo(_ path: String)
}

class DetaileViewObject: NSObject {
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!

    // MARK: - Variables
    private let collectionViewHeight = 100.0
    weak var delegate: DetaileViewObjectDelegate?

    var items: [FeedVideo]!
}

extension DetaileViewObject: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if items.count != 0 {
            collectionViewHeightConstraint.constant = collectionViewHeight
        } else {
            collectionViewHeightConstraint.constant = 0
        }
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoCell.identifire, for: indexPath) as! VideoCell
        cell.item = items[indexPath.row]
        return cell
    }
}

extension DetaileViewObject: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let string = items[indexPath.row].youtubeId {
            delegate?.openVideo(string)
        }
    }
}

extension DetaileViewObject: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewHeightConstraint.constant, height: collectionViewHeightConstraint.constant)
    }
}
