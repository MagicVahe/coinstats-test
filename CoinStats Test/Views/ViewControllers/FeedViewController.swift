//
//  FeedViewController.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import UIKit

class FeedViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    // MARK: - Variables
    private var viewModel: FeedViewModel!
    private let refreshControl = UIRefreshControl()
    private let goToDetaileViewControllerSegue = "goToDetaileViewControllerSegue"
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = FeedViewModel(service: FeedService())
        initTableCell()
        initRefreshControl()
        getData()
    }
    
    // MARK: - Inits
    private func initTableCell() {
        
        tableView.estimatedRowHeight = 20
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib.init(nibName: FeedCell.identifire, bundle: nil),
                           forCellReuseIdentifier: FeedCell.identifire)
        indicator.hidesWhenStopped = true
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        tableView.reloadData()
        if segue.identifier?.compare(goToDetaileViewControllerSegue) == .orderedSame {
            if let dest = segue.destination as? DetaileViewController, let item = sender as? Feed {
                dest.viewModel = DetaileViewModel(item: item)
            }
        }
    }
    
    // MARK: - Request
    private func getData() {
        indicator.startAnimating()
        viewModel.get { [weak self] (isSuccess) in
            if let self = self {
                if isSuccess {
                    self.tableView.reloadData()
                    self.indicator.stopAnimating()
                } else {
                    self.viewModel.fetch()
                    self.tableView.reloadData()
                    self.indicator.stopAnimating()
                }
            }
        }
    }
    
    // MARK: - RefreshControl
    private func initRefreshControl() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.green
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        viewModel.list.removeAll()
        tableView.reloadData()
        getData()
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
    }
}

extension FeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifire) as! FeedCell
        cell.selectionStyle = .none
        cell.item = viewModel.list[indexPath.row]
        return cell
    }
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.list[indexPath.row].isSeen = true
        do {
            try! FeedManager.shared.deleteItem(withID: String(viewModel.list[indexPath.row].date ?? 0))
            try! FeedManager.shared.insert(feed: viewModel.list[indexPath.row])
            try! CoreDataManager.shared.save()
        }
        self.performSegue(withIdentifier: goToDetaileViewControllerSegue, sender: viewModel.list[indexPath.row])
    }
}
