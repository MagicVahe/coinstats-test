//
//  GalleryCell.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import UIKit

class GalleryCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak var displayImageView: UIImageView!
    
    // MARK: - Variables
    static let identifire = "GalleryCell"
    var item: FeedGallery! {
        didSet {
            displayImageView.getImageFromURL(item.thumbnailUrl ?? "")
        }
    }
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        displayImageView?.image = nil
    }

}
