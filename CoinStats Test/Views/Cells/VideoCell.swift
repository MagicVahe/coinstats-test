//
//  VideoCell.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import UIKit

class VideoCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var displayImageView: UIImageView!
    
    // MARK: - Variables
    static let identifire = "VideoCell"
    var item: FeedVideo! {
        didSet {
            displayImageView.getImageFromURL(item.thumbnailUrl ?? "")
        }
    }
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        displayImageView?.image = nil
    }

}
