//
//  FeedCell.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import UIKit
import AlamofireImage

class FeedCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var mainView: BorderedView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var displayImageView: UIImageView!
    
    // MARK: - Variables
    static let identifire = "FeedCell"
    var item: Feed! {
        didSet {
            title.text = item.title
            category.text = item.category
            date.text = DateFormattingHelper.showTime(secondTime: item.date ?? 0)
            displayImageView.getImageFromURL(item.coverPhotoUrl ?? "")
            if item.isSeen {
                mainView.backgroundColor = .white
            } else {
                mainView.backgroundColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1)
            }
        }
    }
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        displayImageView?.image = nil
        title.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
