//
//  GalleryManager.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import CoreData

class GalleryManager {
    
    // MARK: - Variables
    static let shared = GalleryManager()
    var request = NSFetchRequest<GalleryCD>(entityName: "GalleryCD")
    private init() { }
    
    // MARK: - Insert New
    func insert(gallery: FeedGallery, id: String) throws {
        let item = GalleryCD(context: CoreDataManager.shared.context)
        item.id = id
        item.title = gallery.title
        item.thumbnailUrl = gallery.thumbnailUrl
        item.contentUrl = gallery.contentUrl
    }
    
    func save() throws {
        do {
            try CoreDataManager.shared.save()
        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: - Get items
    func fetchAll() throws -> [GalleryCD] {
        let items = try CoreDataManager.shared.context.fetch(GalleryCD.fetchRequest() as NSFetchRequest<GalleryCD>)
        return items
    }

    func fetch(withID itemID: String) throws -> [GalleryCD] {
        request = NSFetchRequest<GalleryCD>(entityName: "GalleryCD")
        request.predicate = NSPredicate(format: "id == %@", itemID)
        let items = try CoreDataManager.shared.context.fetch(request)
        return items
    }
    
    // MARK: - Update item
    func update(item: GalleryCD) throws {
        try! save()
    }
    
    // MARK: - Delete items
    func delete(item: GalleryCD) throws {
        CoreDataManager.shared.context.delete(item)
    }
    
    func deleteItem(withID itemID: String) throws {
        let items = try! fetch(withID: itemID)
        if items.count > 0 {
            try! delete(item: items[0])
        }
    }
    
    func deleteAll() {
        let all = try! fetchAll()
        for item in all {
            try! delete(item: item)
        }
    }
}
