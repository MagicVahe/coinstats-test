//
//  ValidationError.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import Foundation

class ValidationError {
    var type: String = ""
    var title: String = ""
    var detail: String = ""
    var invalidParams: [InvalidParam] = []
    
 
}

class InvalidParam {
    var param: String?
    var code: String?
    var reason: String?
}

class ResponseUnexpectedError {
    var timestamp: String = ""
    var status: Int = 0
    var error: String = ""
    var exception: String = ""
    var message: String = ""
    
  
    
    init(status: Int, error: String, exception: String, message: String) {
        self.status = status
        self.error = error
        self.exception = exception
        self.message = message
    }
    
    static let mappingFailed = ResponseUnexpectedError(
        status: 4003,
        error: "Failed to Map",
        exception: "Mapping",
        message: "Unable to map response data"
    )
    
    static let badRequest = ResponseUnexpectedError(
        status: 4001,
        error: "Failed to request",
        exception: "InvalidRequest",
        message: "Something wrong was done in my end"
    )
}
