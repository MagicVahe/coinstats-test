//
//  BaseService.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import Foundation
import Alamofire

struct Config {
    static let HOST_NAME = "https://coinstats.getsandbox.com"
    static let BASE_URL = Config.HOST_NAME
}

struct CustomGetEncoding: ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try URLEncoding().encode(urlRequest, with: parameters)
        request.url = URL(string: request.url!.absoluteString.replacingOccurrences(of: "%5B%5D=", with: "="))
        return request
    }
}

/*
 ServiceState state.
 */
enum ServiceState {
    case online
    case offline
}

enum RequestType {
    case secure
    case unsecure
}

enum AcceptSpecifier: String {
    case `default` = ""
    case special = ".special"
}

class BaseService {
    public static let shared = BaseService()
    func getHeaders(for: RequestType, specifier: AcceptSpecifier = .default) -> [String: String] {
        return [
            "Content-Type": "application/json"
        ]
    }
    
    func post(
        endpoint: String,
        object: [String: Any],
        for type: RequestType = .secure
        ) -> DataRequest {
        return request(Config.HOST_NAME + endpoint,
                       method: .post, parameters: object)
    }
    
    func get(
        endpoint: String,
        parameters: [String: Any],
        for type: RequestType = .secure,
        specifier: AcceptSpecifier = .default
        ) -> DataRequest {
        return request(Config.HOST_NAME + endpoint,
                       method: .get,
                       parameters: parameters,
                       encoding: CustomGetEncoding(),
                       headers: getHeaders(for: type, specifier: specifier)
        )
    }

    // MARK: - Checking base response cases
    func checkBaseResponse(_ httpResponse: HTTPURLResponse, _ string: String) -> BaseResponse? {
        switch httpResponse.statusCode {
        case 400:
            return .badRequest
        case 401:
            return .unauthorized
        case 404:
            return .notFound
        case 422:
            return .unexpectedError(error: ResponseUnexpectedError.mappingFailed)
        case 500..<600:
            return .unexpectedError(error: ResponseUnexpectedError.mappingFailed)
        default:
            return nil
        }
    }
}
