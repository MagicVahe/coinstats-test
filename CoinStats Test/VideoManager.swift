//
//  VideoManager.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation
import CoreData

class VideoManager {
    
    // MARK: - Variables
    static let shared = VideoManager()
    var request = NSFetchRequest<VideoCD>(entityName: "VideoCD")
    private init() { }
    
    // MARK: - Insert New
    func insert(video: FeedVideo, id: String) throws {
        let item = VideoCD(context: CoreDataManager.shared.context)
        item.id = id
        item.title = video.title
        item.thumbnailUrl = video.thumbnailUrl
        item.youtubeId = video.youtubeId
    }
    
    func save() throws {
        do {
            try CoreDataManager.shared.save()
        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: - Get items
    func fetchAll() throws -> [VideoCD] {
        let items = try CoreDataManager.shared.context.fetch(VideoCD.fetchRequest() as NSFetchRequest<VideoCD>)
        return items
    }

    func fetch(withID itemID: String) throws -> [VideoCD] {
        request = NSFetchRequest<VideoCD>(entityName: "VideoCD")
        request.predicate = NSPredicate(format: "id == %@", itemID)
        let items = try CoreDataManager.shared.context.fetch(request)
        return items
    }
    
    // MARK: - Update Item
    func update(item: VideoCD) throws {
        try! save()
    }
    
    // MARK: - Delete Item
    func delete(item: VideoCD) throws {
        CoreDataManager.shared.context.delete(item)
    }
    
    func deleteItem(withID itemID: String) throws {
        let items = try! fetch(withID: itemID)
        if items.count > 0 {
            try! delete(item: items[0])
        }
    }
    
    func deleteAll() {
        let all = try! fetchAll()
        for item in all {
            try! delete(item: item)
        }
    }
}
