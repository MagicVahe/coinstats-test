//
//  DetaileViewModel.swift
//  CoinStats Test
//
//  Created by Vahe on 21.04.22.
//

import Foundation

class DetaileViewModel {
    // MARK: - Variables
    var item: Feed!
    
    // MARK: - init
    init(item: Feed) {
        self.item = item
    }
}
