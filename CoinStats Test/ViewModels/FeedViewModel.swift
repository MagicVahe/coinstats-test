//
//  FeedViewModel.swift
//  CoinStats Test
//
//  Created by Vahe on 20.04.22.
//

import Foundation

class FeedViewModel {
    // MARK: - Variables
    var service: FeedServiceProtocol!
    var list = [Feed]()
    
    // MARK: - init
    init(service: FeedServiceProtocol) {
        self.service = service
    }
    
    func get(completion: @escaping ((Bool) -> Void)) {
        service.get() { [unowned self] (response) in
            switch response {
            case .base(response: _):
                completion(false)
                break
            case .success(data: let data):
                self.list += data.data ?? []
                if list.count > 0 {
                    addSeen()
                    deleteAll()
                    saveInCoreData()
                }
                completion(true)
            }
        }
    }
    
    private func addSeen() {
        do {
            let feedArray = try! FeedManager.shared.fetchAll()
            for item in feedArray {
                for itemL in list {
                    if item.id == String(itemL.date ?? 0) {
                        itemL.isSeen = item.isSeen
                    }
                }
            }
        }
    }
    
    private func saveInCoreData() {
        for item in list {
            do {
                try! FeedManager.shared.insert(feed: item)
            }
            if let gallery = item.gallery {
                for itemG in gallery {
                    do {
                        try! GalleryManager.shared.insert(gallery: itemG, id: String(item.date ?? 0))
                    }
                }
            }
            if let video = item.video {
                for itemV in video {
                    do {
                        try! VideoManager.shared.insert(video: itemV, id: String(item.date ?? 0))
                    }
                }
            }
        }
        try! CoreDataManager.shared.save()
    }
    
    private func deleteAll() {
        FeedManager.shared.deleteAll()
        GalleryManager.shared.deleteAll()
        VideoManager.shared.deleteAll()
    }
    
    func fetch() {
        list.removeAll()
        do {
            let feed = try! FeedManager.shared.fetchAll()
            for item in feed {
                let itemF = Feed()
                itemF.category = item.category
                itemF.title = item.title
                itemF.body = item.body
                itemF.shareUrl = item.shareUrl
                itemF.coverPhotoUrl = item.coverPhotoUrl
                itemF.date = Int(item.date ?? "0")
                itemF.isSeen = item.isSeen
                
                let galler = try! GalleryManager.shared.fetch(withID: item.id ?? "")
                if galler.count > 0 {
                    itemF.gallery = [FeedGallery]()
                    for itemG in galler {
                        let itemFeedGallery = FeedGallery()
                        itemFeedGallery.title = itemG.title
                        itemFeedGallery.thumbnailUrl = itemG.thumbnailUrl
                        itemFeedGallery.contentUrl = itemG.contentUrl
                        itemF.gallery?.append(itemFeedGallery)
                    }
                }
                let video = try! VideoManager.shared.fetch(withID: item.id ?? "")
                if video.count > 0 {
                    itemF.video = [FeedVideo]()
                    for itemV in video {
                        let itemFeedVideo = FeedVideo()
                        itemFeedVideo.title = itemV.title
                        itemFeedVideo.thumbnailUrl = itemV.thumbnailUrl
                        itemFeedVideo.youtubeId = itemV.youtubeId
                        itemF.video?.append(itemFeedVideo)
                    }
                }
                list.append(itemF)
            }
        }
    }
}
